package io.quind.model.ci.jenkins;

public class JenkinsJobBuild {

    private String _class;
    private int number;
    private String url;

    public JenkinsJobBuild() {
    }

    public JenkinsJobBuild(String _class, int number, String url) {
        this._class = _class;
        this.number = number;
        this.url = url;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
