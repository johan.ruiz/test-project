package io.quind.client.ci.adapters.jenkins;

import io.quind.model.ci.jenkins.*;
import io.quind.model.quind.jobs.JobDefinition;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


public class JenkinsAdapter {

    public List<JobDefinition> parseBuildDefinition(JenkinsJobDefinition jenkinsJobDefinition, Response buildDefinitionResponse) {
        JenkinsBuildDefinition build = new JenkinsBuildDefinition();
        try {
            build = buildDefinitionResponse.readEntity(JenkinsBuildDefinition.class);
            buildDefinitionResponse = null;
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println(buildDefinitionResponse.toString());
        }

        List<JobDefinition> jobs = new ArrayList<>();
        fillJobDefinition(build, fixFullDisplayName(jenkinsJobDefinition.getFullDisplayName()), jobs);
        return jobs;
    }

    private String fixFullDisplayName(String fullDisplayName) {
        return fullDisplayName.replace(" » ", "->");
    }

    public void fillJobDefinition(JenkinsBuildDefinition lastBuild, String displayName, List<JobDefinition> jobs) {
        if(lastBuild != null) {
            List<Action> actions = lastBuild.getActions();
            for (Action action : actions) {
                if (action != null && action.getClass_() != null) {
                    //Este código permite analizar la información cuando se implementa el PlugIn Pipeline
                    if (action.getNodes() != null && action.getNodes().size() > 0) {
                        List<String> jobNames = getJobs(action);
                        for(String jobName : jobNames) {
                            JobDefinition jobDefinition = new JobDefinition();
                            jobDefinition.setType("build");
                            jobDefinition.setName(displayName + jobName);
                            jobs.add(jobDefinition);
                        }
                    } else {
                        JobDefinition jobDefinition = new JobDefinition();
                        jobDefinition.setType("build");
                        jobDefinition.setName(displayName + "->" + action.getClass_());
                        jobs.add(jobDefinition);
                    }
                }
            }
        }
    }

    public List<String> getJobs(Action action) {
        String jobName = "";
        List<String> jobNames = new ArrayList<>();
        for(Node node : action.getNodes()) {
            if("org.jenkinsci.plugins.workflow.cps.nodes.StepStartNode".equals(node.getClass_())) {
                jobName = jobName + "->" + node.getDisplayName();
            } else if ("org.jenkinsci.plugins.workflow.cps.nodes.StepAtomNode".equals(node.getClass_())){
                jobNames.add(jobName + "->" + node.getDisplayName());
            } else if ("org.jenkinsci.plugins.workflow.cps.nodes.StepEndNode".equals(node.getClass_())) {
                jobName = jobName.substring(0,jobName.lastIndexOf("->"));
            }
        }
        return jobNames;
    }

    public List<JobDefinition> parseReleaseDefinitions(Response releaseResponse) {
        return null;
    }

    public List<JobDefinition> parseReleaseDetailsDefinitions(JobDefinition jobDefinitionBase, Response releaseDetailsResponse) {
        return null;
    }
}
