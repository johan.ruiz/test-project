package io.quind.client.ci.facade;

import java.net.URISyntaxException;
import java.util.*;

import io.quind.client.ci.adapters.jenkins.JenkinsAdapter;
import io.quind.client.ci.adapters.tfs.TFSAdapter;
import io.quind.client.ci.adapters.vsts.VSTSAdapter;
import io.quind.client.ci.collectors.CICollector;
import io.quind.client.ci.collectors.jenkins.JenkinsCollector;
import io.quind.client.helpers.ConstantDataManager;
import io.quind.client.ci.repository.jenkins.JenkinsRepository;
import io.quind.client.ci.repository.tfs.TFSRepository;
import io.quind.client.ci.repository.vsts.VSTSRepository;
import io.quind.client.facade.CollectorFacade;
import io.quind.model.exceptions.QuindException;
import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.servers.TypeOfBuildServer;
import io.quind.repository.QuindRepository;
import io.quind.rest.RestClient;

public class ClientFacade extends CollectorFacade {

    private QuindRepository quindRepository;
    private JenkinsRepository jenkinsRepository;
    private TFSRepository tfsRepository;
    private VSTSRepository vstsRepository;
    private TFSAdapter tfsAdapter = new TFSAdapter();
    private VSTSAdapter vstsAdapter = new VSTSAdapter();
    private JenkinsAdapter jenkinsAdapter = new JenkinsAdapter();
    private CICollector vstsCollector;
    private JenkinsCollector jenkinsCollector;
    private CICollector tfsCollector;
    private RestClient restClient;

    private final String TFS = "tfs";
    private final String JENKINS = "jenkins";
    private final String VSTS = "vsts";

    public ClientFacade(RestClient restClient, QuindRepository quindRepository) {
        this.quindRepository = quindRepository;
        this.restClient = restClient;
        this.jenkinsRepository = new JenkinsRepository(restClient, jenkinsAdapter);
        this.tfsRepository = new TFSRepository(restClient, tfsAdapter);
        this.vstsRepository = new VSTSRepository(restClient, vstsAdapter);
        this.vstsCollector = new CICollector(vstsRepository);
        this.jenkinsCollector = new JenkinsCollector(jenkinsRepository);
        this.tfsCollector = new CICollector(tfsRepository);
    }

    private void evictJenkinsCache() {
        this.jenkinsRepository = new JenkinsRepository(restClient, jenkinsAdapter);
        this.jenkinsCollector = new JenkinsCollector(jenkinsRepository);
    }

    public void collectAndSendInformation() {
        evictJenkinsCache();
        List<Integer> apiClients = quindRepository.getApiClients(ConstantDataManager.API_CLIENTS_TOKEN());
        try {
            collectAndSendInformation(apiClients);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void collectAndSendInformation(List<Integer> apiClients) throws URISyntaxException {
        for (int apiClient : apiClients) {
            List<ApplicationConfig> applicationConfigs = quindRepository.getProviderConfiguration(apiClient);
            Map<String, List<ApplicationConfig>> appConfigsSplitted = splitAppConfigs(applicationConfigs);
            List<ApplicationConfig> jenkinsConfigs = appConfigsSplitted.get(JENKINS);
            List<ApplicationConfig> vstsConfigs = appConfigsSplitted.get(VSTS);
            List<ApplicationConfig> tfsConfigs = appConfigsSplitted.get(TFS);

            for(ApplicationConfig vstsConfig : vstsConfigs) {
                List<Application> applicationsVSTS = collectInformation(Arrays.asList(vstsConfig), "V");
                sendInformation(applicationsVSTS);
            }
            for(ApplicationConfig tfsConfig : tfsConfigs) {
                List<Application> applicationsTFS = collectInformation(Arrays.asList(tfsConfig), "T");
                sendInformation(applicationsTFS);
            }
            for(ApplicationConfig jenkisConfig : jenkinsConfigs) {
                List<Application> applicationsJenkins = collectInformation(Arrays.asList(jenkisConfig), "J");
                sendInformation(applicationsJenkins);
            }
            finishProcess(apiClient);
        }
    }

    private List<Application> collectInformation(List<ApplicationConfig> applicationConfigs, String type) throws URISyntaxException {
        List<Application> applications = new ArrayList<>();
        if(applicationConfigs != null && applicationConfigs.size() > 0) {
            if("J".equals(type)) {
                applications.addAll(jenkinsCollector.collectJobDefinitions(applicationConfigs));
            }
            if("T".equals(type)) {
                applications.addAll(tfsCollector.collectJobDefinitions(applicationConfigs));
            }
            if("V".equals(type)) {
                applications.addAll(vstsCollector.collectJobDefinitions(applicationConfigs));
            }
        }
        return applications;
    }

    private void sendInformation(List<Application> applications) {
        if (applications.size() > 0) {
            boolean postJobsResult = quindRepository.enviarJobDefinitions(applications);
            if (!postJobsResult) {
                throw new QuindException("Error enviando la información a Quind");
            }
        }
    }

    private void finishProcess(int apiClient) {
        boolean postFinishResult = quindRepository.finishProcess(apiClient);
        if (!postFinishResult) {
            throw new QuindException("Error finalizando el proceso en Quind");
        }
    }

    private Map<String, List<ApplicationConfig>> splitAppConfigs(List<ApplicationConfig> appConfigs) {
        Map<String, List<ApplicationConfig>> result = new HashMap<>();
        List<ApplicationConfig> jenkinsConfigs = new ArrayList<>();
        List<ApplicationConfig> vstsConfigs = new ArrayList<>();
        List<ApplicationConfig> tfsConfigs = new ArrayList<>();

        for (ApplicationConfig appConfig : appConfigs) {
            if (TypeOfBuildServer.VSTS.getCode() == appConfig.getTypeOfBuildServer()) {
                vstsConfigs.add(appConfig);
            } else if (TypeOfBuildServer.TFS.getCode() == appConfig.getTypeOfBuildServer()) {
                tfsConfigs.add(appConfig);
            } else if (TypeOfBuildServer.JENKINS.getCode() == appConfig.getTypeOfBuildServer()) {
                jenkinsConfigs.add(appConfig);
            }
        }
        result.put(JENKINS, jenkinsConfigs);
        result.put(VSTS, vstsConfigs);
        result.put(TFS, tfsConfigs);
        return result;
    }
}
