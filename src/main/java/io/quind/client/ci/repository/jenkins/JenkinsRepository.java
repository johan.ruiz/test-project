package io.quind.client.ci.repository.jenkins;

import io.quind.client.ci.adapters.jenkins.JenkinsAdapter;
import io.quind.client.ci.helpers.ApplicationConfigHelper;
import io.quind.model.ci.jenkins.JenkinsJobBuild;
import io.quind.model.ci.jenkins.JenkinsJobDefinition;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;
import io.quind.rest.RestClient;

import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JenkinsRepository {

    private RestClient restClient;
    private JenkinsAdapter jenkinsAdapter;
    private Map<String, JenkinsJobDefinition> cachedJobDefinitions;
    private Map<String, List<JobDefinition>> cachedBuildDefinitions;

    public JenkinsRepository(RestClient restClient, JenkinsAdapter jenkinsAdapter) {
        this.restClient = restClient;
        this.jenkinsAdapter = jenkinsAdapter;
        this.cachedJobDefinitions = new HashMap<>();
        this.cachedBuildDefinitions = new HashMap<>();
    }

    public JenkinsJobDefinition getJobDefinition(ApplicationConfig applicationConfig) {
        Response buildResponse = null;
        JenkinsJobDefinition jenkinsJob = new JenkinsJobDefinition();
        try {
            String jobURL = applicationConfig.getUrlBuildServer() + applicationConfig.getJobsPath() + "/api/json";
            if(!cachedJobDefinitions.containsKey(jobURL)) {
                buildResponse = restClient.doGet(jobURL,
                        applicationConfig.getUsernameBuildServer(), applicationConfig.getTokenBuildServer());
                if (buildResponse.getStatus() != 200) {
                    throw new Exception(buildResponse.toString());
                }
                jenkinsJob = buildResponse.readEntity(JenkinsJobDefinition.class);
                cachedJobDefinitions.put(jobURL, jenkinsJob);
            } else {
                System.out.println("Cached: " + jobURL);
                jenkinsJob = cachedJobDefinitions.get(jobURL);
            }
        } catch (Exception e) {
            e.printStackTrace(); // El proceso debe continuar para recolectar la información de los otros
            // servicios.
        }
        return jenkinsJob;
    }

    public List<JobDefinition> getBuildDefinition(JenkinsJobDefinition jenkinsJobDefinition, JenkinsJobBuild jenkinsBuildDefinition, ApplicationConfig appConfig) throws URISyntaxException {
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        String buildURL = appConfig.getUrlBuildServer() + ApplicationConfigHelper.getJobPath(jenkinsBuildDefinition.getUrl()) + "/api/json?depth=1&tree=displayName,fullDisplayName,actions[_class,nodes[displayName,id,parents,_class]]";
        if(!cachedBuildDefinitions.containsKey(buildURL)) {
            Response responseBuildDefinition = restClient.doGet(buildURL,
                    appConfig.getUsernameBuildServer(), appConfig.getTokenBuildServer());
            jobDefinitions.addAll(jenkinsAdapter.parseBuildDefinition(jenkinsJobDefinition, responseBuildDefinition));
            cachedBuildDefinitions.put(buildURL, jobDefinitions);
        } else {
            System.out.println("Cached: " + buildURL);
            jobDefinitions = cachedBuildDefinitions.get(buildURL);
        }

        return jobDefinitions;
    }
}
