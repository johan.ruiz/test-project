package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildPlatform {

    private String value;
    private String allowOverride;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAllowOverride() {
        return allowOverride;
    }

    public void setAllowOverride(String allowOverride) {
        this.allowOverride = allowOverride;
    }
}
