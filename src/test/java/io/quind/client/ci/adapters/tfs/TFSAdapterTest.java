package io.quind.client.ci.adapters.tfs;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.mockito.Mockito;

import io.quind.model.ci.microsoft.build.tfs.Build;
import io.quind.model.ci.microsoft.build.tfs.Value;
import io.quind.model.ci.microsoft.build.tfs.details.BuildDetails;
import io.quind.model.ci.microsoft.build.tfs.details.Step;
import io.quind.model.ci.microsoft.release.tfs.Release;
import io.quind.model.ci.microsoft.release.tfs.details.DeployStep;
import io.quind.model.ci.microsoft.release.tfs.details.Environment;
import io.quind.model.ci.microsoft.release.tfs.details.ReleaseDetails;
import io.quind.model.ci.microsoft.release.tfs.details.Task;
import io.quind.model.quind.jobs.JobDefinition;

@SuppressWarnings("unchecked")
public class TFSAdapterTest {

	TFSAdapter adapter = new TFSAdapter();
	
	@Test
    public void shouldParseBuildDefinitions() {
		Value value = new Value();
		value.setId(2);
        value.setName("User Model Phase 1");
        value.setType("build");
        value.setUrl("http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2");
		List<Value> values = new ArrayList<>();
		values.add(value);
		
		Build build = new Build();
		build.setValue(values);
		final Response buildResponse = Mockito.mock(Response.class);
        Mockito.when(buildResponse.readEntity(Mockito.any(Class.class))).thenReturn(build);
    	
        List<JobDefinition> expectedJobs = new ArrayList<>();
        expectedJobs.add(new JobDefinition("2", "User Model Phase 1", "build", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2"));
        List<JobDefinition> result = adapter.parseBuildDefinitions(buildResponse);
        assertEquals(expectedJobs.size(), result.size());
        assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
        assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
        assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
        assertEquals(expectedJobs.get(0).getUrlForDetails(), result.get(0).getUrlForDetails());
    }

	@Test
    public void shouldParseReleaseDefinitions() {
    	io.quind.model.ci.microsoft.release.tfs.Value value = new io.quind.model.ci.microsoft.release.tfs.Value();
		value.setId(5);
        value.setName("Release Location API");
        value.setUrl("http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5");
		List<io.quind.model.ci.microsoft.release.tfs.Value> values = new ArrayList<>();
		values.add(value);
		
    	Release release = new Release();
    	release.setValue(values);
    	final Response releaseResponse = Mockito.mock(Response.class);
        Mockito.when(releaseResponse.readEntity(Mockito.any(Class.class))).thenReturn(release);
    	
        List<JobDefinition> expectedJobs = new ArrayList<>();
        expectedJobs.add(new JobDefinition("5", "Release Location API", "release", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5"));
        List<JobDefinition> result = adapter.parseReleaseDefinitions(releaseResponse);
        assertEquals(expectedJobs.size(), result.size());
        assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
        assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
        assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
        assertEquals(expectedJobs.get(0).getUrlForDetails(), result.get(0).getUrlForDetails());
    }

	@Test
    public void shouldParseBuildDetailsDefinitions() {
    	JobDefinition jobDefinitionBase = new JobDefinition("2", "User Model Phase 1", "build", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2");
    	
    	Step step = new Step();
    	step.setDisplayName("NuGet restore **\\\\*.sln");
    	List<Step> build = new ArrayList<>();
    	build.add(step);
    	BuildDetails buildDetails = new BuildDetails();
    	buildDetails.setBuild(build);
    	
    	final Response buildDetailsResponse = Mockito.mock(Response.class);
        Mockito.when(buildDetailsResponse.readEntity(Mockito.any(Class.class))).thenReturn(buildDetails);

        List<JobDefinition> expectedJobs = new ArrayList<>();
        expectedJobs.add(new JobDefinition("2", "User Model Phase 1->NuGet restore **\\\\*.sln", "build", null));
        List<JobDefinition> result = adapter.parseBuildDetailsDefinitions(jobDefinitionBase, buildDetailsResponse);
        assertEquals(expectedJobs.size(), result.size());
        assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
        assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
        assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
    }

	@Test
    public void shouldParseReleaseDetailsDefinitions() {
    	JobDefinition jobDefinitionBase = new JobDefinition("5", "Release Location API", "release", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5");

    	Task task = new Task();
    	task.setName("Release Test");
    	List<Task> tasks = new ArrayList<>();
    	tasks.add(task);
    	DeployStep deploymentStep = new DeployStep();
    	deploymentStep.setTasks(tasks);
    	Environment environment = new Environment();
    	environment.setName("Development");
    	environment.setDeployStep(deploymentStep);
    	List<Environment> environments = new ArrayList<>();
    	environments.add(environment);
        ReleaseDetails releaseDetails = new ReleaseDetails();
        releaseDetails.setEnvironments(environments);
     	
        final Response releaseDetailsResponse = Mockito.mock(Response.class);
        Mockito.when(releaseDetailsResponse.readEntity(Mockito.any(Class.class))).thenReturn(releaseDetails);
 	
        List<JobDefinition> expectedJobs = new ArrayList<>();
        expectedJobs.add(new JobDefinition("5", "Release Location API->Development->Release Test", "release", null));
        List<JobDefinition> result = adapter.parseReleaseDetailsDefinitions(jobDefinitionBase, releaseDetailsResponse);
        assertEquals(expectedJobs.size(), result.size());
        assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
        assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
        assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
    }
	
}
