package io.quind.client.ci.collectors;

import io.quind.client.ci.helpers.ApplicationConfigHelper;
import io.quind.client.ci.repository.CIRepository;
import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CICollector {

    private CIRepository ciRepository;

    protected CICollector() {
    }

    public CICollector(CIRepository ciRepository) {
        this.ciRepository = ciRepository;
    }

    public List<Application> collectJobDefinitions(List<ApplicationConfig> applicationConfigs) {
        Map<String, List<ApplicationConfig>> configsByServer = ApplicationConfigHelper.groupAppConfigByServer(applicationConfigs);
        List<Application> applications = new ArrayList<>();
        Map<String, List<JobDefinition>> jobDefsByApp = new HashMap<>();

        configsByServer.keySet().forEach(key -> {
            try {
                collectJobDefinitionsByServer(configsByServer, applications, jobDefsByApp, key);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        });
        return applications;
    }

    protected void collectJobDefinitionsByServer(Map<String, List<ApplicationConfig>> configsByServer, List<Application> applications, Map<String, List<JobDefinition>> jobDefsByApp, String key) throws URISyntaxException {
        List<ApplicationConfig> appsConfig = configsByServer.get(key);
        ApplicationConfig appConfig = appsConfig.iterator().next();
        List<JobDefinition> jobDefinitions = ciRepository.getJobDefinitions(appConfig);

        List<JobDefinition> jobDefinitionsDetails = new ArrayList<>();
        List<JobDefinition> jobDefinitionsComplete = new ArrayList<>();
        for (JobDefinition jobDef : jobDefinitions) {
            jobDefinitionsDetails = ciRepository.getJobDefinitionsDetails(jobDef, appConfig);
            for(JobDefinition jobDefinitionsDetail: jobDefinitionsDetails) {
                if(jobDefinitionsDetail.isYaml()) {
                    jobDefinitionsComplete.addAll(ciRepository.getJobDefinitionsFromYaml(jobDef, jobDefinitionsDetail, appConfig));
                } else {
                    jobDefinitionsComplete.add(jobDefinitionsDetail);
                }
            }
            ApplicationConfigHelper.buildJobDefs(jobDefsByApp, appsConfig, jobDefinitionsComplete, jobDef);
        }
        ApplicationConfigHelper.setApplicationJobDefinitions(applications, jobDefsByApp);
    }
}
