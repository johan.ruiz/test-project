package io.quind.client.ci.repository.tfs;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import io.quind.client.ci.adapters.tfs.TFSAdapter;
import io.quind.model.ci.microsoft.build.tfs.Build;
import io.quind.model.ci.microsoft.build.tfs.Value;
import io.quind.model.ci.microsoft.build.tfs.details.BuildDetails;
import io.quind.model.ci.microsoft.build.tfs.details.Step;
import io.quind.model.ci.microsoft.release.tfs.Release;
import io.quind.model.ci.microsoft.release.tfs.details.DeployStep;
import io.quind.model.ci.microsoft.release.tfs.details.Environment;
import io.quind.model.ci.microsoft.release.tfs.details.ReleaseDetails;
import io.quind.model.ci.microsoft.release.tfs.details.Task;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;
import io.quind.rest.RestClient;

public class TFSRepositoryTest {

	@Mock
	RestClient restClient;

	TFSAdapter ciAdapter = new TFSAdapter();
	TFSRepository tfsRepository;

	public TFSRepositoryTest() {
		MockitoAnnotations.initMocks(this);
		tfsRepository = new TFSRepository(restClient, ciAdapter);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldGetJobDefinitions() {
		Value value = new Value();
		value.setId(2);
		value.setName("User Model Phase 1");
		value.setType("build");
		value.setUrl(
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2");
		List<Value> values = new ArrayList<>();
		values.add(value);

		Build build = new Build();
		build.setValue(values);
		final Response buildResponse = Mockito.mock(Response.class);
		Mockito.when(buildResponse.readEntity(Mockito.any(Class.class))).thenReturn(build);

		io.quind.model.ci.microsoft.release.tfs.Value releaseValue = new io.quind.model.ci.microsoft.release.tfs.Value();
		releaseValue.setId(5);
		releaseValue.setName("Release Location API");
		releaseValue.setUrl(
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5");
		List<io.quind.model.ci.microsoft.release.tfs.Value> releaseValues = new ArrayList<>();
		releaseValues.add(releaseValue);

		Release release = new Release();
		release.setValue(releaseValues);
		final Response releaseResponse = Mockito.mock(Response.class);
		Mockito.when(releaseResponse.readEntity(Mockito.any(Class.class))).thenReturn(release);

		doReturn(buildResponse).when(restClient).doGet(
				"http://fakeurl.build.tfs.com/fakepath/_apis/build/definitions?api-version=2.0", "username",
				"password");

		doReturn(releaseResponse).when(restClient)
				.doGet("http://fakeurl.release.tfs.com/fakepath/_apis/release/definitions", "username", "password");

		List<JobDefinition> expectedJobs = new ArrayList<>();
		expectedJobs.add(new JobDefinition("2", "User Model Phase 1", "build",
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2"));
		expectedJobs.add(new JobDefinition("5", "Release Location API", "release",
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5"));

		List<JobDefinition> result = tfsRepository
				.getJobDefinitions(new ApplicationConfig("1", "fakepath", "expression", "http://fakeurl.build.tfs.com",
						"http://fakeurl.release.tfs.com", 2, "username", "password", "username", "password", "Name"));
		assertEquals(expectedJobs.size(), result.size());
		assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
		assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
		assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
		assertEquals(expectedJobs.get(0).getUrlForDetails(), result.get(0).getUrlForDetails());

		assertEquals(expectedJobs.size(), result.size());
		assertEquals(expectedJobs.get(1).getId(), result.get(1).getId());
		assertEquals(expectedJobs.get(1).getName(), result.get(1).getName());
		assertEquals(expectedJobs.get(1).getType(), result.get(1).getType());
		assertEquals(expectedJobs.get(1).getUrlForDetails(), result.get(1).getUrlForDetails());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldGetBuildJobDefinitionsDetails() {
		JobDefinition jobDefinitionBase = new JobDefinition("2", "User Model Phase 1", "build",
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2");

		Step step = new Step();
		step.setDisplayName("NuGet restore **\\\\*.sln");
		List<Step> build = new ArrayList<>();
		build.add(step);
		BuildDetails buildDetails = new BuildDetails();
		buildDetails.setBuild(build);

		final Response buildDetailsResponse = Mockito.mock(Response.class);
		Mockito.when(buildDetailsResponse.readEntity(Mockito.any(Class.class))).thenReturn(buildDetails);

		doReturn(buildDetailsResponse).when(restClient).doGet(
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/2",
				"username", "password");

		List<JobDefinition> expectedJobs = new ArrayList<>();
		expectedJobs.add(new JobDefinition("2", "User Model Phase 1->NuGet restore **\\\\*.sln", "build", null));
		List<JobDefinition> result = tfsRepository.getJobDefinitionsDetails(jobDefinitionBase,
				new ApplicationConfig("1", "fakepath", "expression", "http://fakeurl.build.tfs.com",
						"http://fakeurl.release.tfs.com", 2, "username", "password", "username", "password", "Name"));
		assertEquals(expectedJobs.size(), result.size());
		assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
		assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
		assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldGetReleaseJobDefinitionsDetails() {
		JobDefinition jobDefinitionBase = new JobDefinition("5", "Release Location API", "release",
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5");

		Task task = new Task();
		task.setName("Release Test");
		List<Task> tasks = new ArrayList<>();
		tasks.add(task);
		DeployStep deploymentStep = new DeployStep();
		deploymentStep.setTasks(tasks);
		Environment environment = new Environment();
		environment.setName("Development");
		environment.setDeployStep(deploymentStep);
		List<Environment> environments = new ArrayList<>();
		environments.add(environment);
		ReleaseDetails releaseDetails = new ReleaseDetails();
		releaseDetails.setEnvironments(environments);

		final Response releaseDetailsResponse = Mockito.mock(Response.class);
		Mockito.when(releaseDetailsResponse.readEntity(Mockito.any(Class.class))).thenReturn(releaseDetails);

		doReturn(releaseDetailsResponse).when(restClient).doGet(
				"http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/5",
				"username", "password");

		List<JobDefinition> expectedJobs = new ArrayList<>();
		expectedJobs.add(new JobDefinition("5", "Release Location API->Development->Release Test", "release", null));
		List<JobDefinition> result = tfsRepository.getJobDefinitionsDetails(jobDefinitionBase,
				new ApplicationConfig("1", "fakepath", "expression", "http://fakeurl.build.tfs.com",
						"http://fakeurl.release.tfs.com", 2, "username", "password", "username", "password", "Name"));
		assertEquals(expectedJobs.size(), result.size());
		assertEquals(expectedJobs.get(0).getId(), result.get(0).getId());
		assertEquals(expectedJobs.get(0).getName(), result.get(0).getName());
		assertEquals(expectedJobs.get(0).getType(), result.get(0).getType());
	}
}
