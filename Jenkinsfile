@Library('shared-libraries') _

// This shows a simple build wrapper example, using the AnsiColor plugin.
	pipeline { 
	  agent none

		environment {
			// deploy_version = "${sh(returnStdout: true, script: 'echo $(./Salary.Scripts/get-git-version.sh)')}"
			id = "i-0d44457e611b1da91"
			production_EC2_machine_ip = "52.38.27.192"
			service_name = "cilog-client"
			service_script_name = "CilogClient"
			git_credential_id = "quind"
		}

		triggers { 
			bitbucketPush()
			}
			
    	tools {
			maven "MAVEN"
		}

		stages {	
			
			stage('Get artifact version'){
				agent any
				steps{
					script{
						deploy_version = """${sh(returnStdout: true, script: 'echo $(gitversion)').trim()}"""
					}
				}
			}

			stage ('Generating Quind Client Artifact') {
				agent any
				steps {
					generateQuindClientArtifact("$git_credential_id")
				}
			}

			stage ('Unit Tests Stage') {
				agent any
				steps {
					sh 'mvn test'
				}
			}

			stage('Static Code Analysis') {
				agent any
				steps {
					withSonarQubeEnv('sonarqube') {
						sh 'mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent package sonar:sonar'
					}
				}
			}

			stage('Quality Gate') {
				agent any
				steps {
					script{
							timeout(time: 10, unit: 'MINUTES') { // Just in case something goes wrong, pipeline will be killed after a timeout
							def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
							if (qg.status != 'OK') {
								error "Pipeline aborted due to quality gate failure: ${qg.status}"
							}
						}
					}
				}
			}

      stage ('Build Project') {
				agent any
		  	steps {
					sh 'mvn clean package'
				}
		 	}

			stage('Staging: Turn On EC2 Instance'){
				agent any
				steps {
					script {
						stagingEC2MachineIP = turnOnEC2Instance('quind-aws',"$id")
					}
				}
			}

			stage('Staging: Copy files to Staging EC2 Machine') {
				agent any
				steps {
					echo "stagingEC2MachineIP: ${stagingEC2MachineIP}"
					script {
						last_stable_version = copyArtifactToEC2Instance('staging-agent', "${stagingEC2MachineIP}", "$service_name", "$deploy_version", "$service_script_name")
					}
				}
			}

			stage('Staging: Star Service') {
				agent any
				steps {
					startServiceOnEC2Instance('staging-agent', "${stagingEC2MachineIP}", "$service_script_name")
				}
			}

			stage('Staging: Service Smoke Tests') {
				agent any
				steps {
					clientServiceSmokeTests('staging-agent', "${stagingEC2MachineIP}", "$service_name", "$deploy_version")					
				}
			}

			stage('Staging: Smoke test Failed - Roll Back Version') {
				agent any
				when {
					expression {
						currentBuild.result == null || currentBuild.result == 'FAILURE'
					}
				}
				steps {
					clientServiceRollBackVersion('staging-agent', "${stagingEC2MachineIP}", "$service_name", "$deploy_version", "$service_script_name")
					echo "Check Smoke Tests Stage, there were a problem with your Last Versioned Artifact"
				}
			}

			stage('Staging: Smoke test Successed - Updating Service Git Repository and S3') {
				agent any
				when {
				expression {
					currentBuild.result == null || currentBuild.result == 'SUCCESS'
				}
				}
				steps {
					updateClientServiceGitAndS3("$git_credential_id", 'quind-aws', 'staging-agent', "$service_name", "$deploy_version", "$service_script_name", "s3://cilog-qalog-clients/staging")	
				}
			}

			//
			// stage('Production: Manual approval') {
			// 	agent none
			// 	steps {
			// 		timeout(10) {
			// 			input message:'Approve deployment?', submitter: 'it-ops'
			// 		}
			// 		echo "Approved!"
			// 	}
			// }
			
			

			// stage('Production: Copy files EC2 Machine') {
			// 	agent any
			// 	steps {
			// 		script {
			// 			echo "stagingEC2MachineIP: ${production_EC2_machine_ip}"
			// 			// last_stable_version = copyArtifactToEC2Instance('production-agent', "${production_EC2_machine_ip}", "$service_name", "$deploy_version", "$service_script_name")
			// 		}
			// 	}
			// }

			// stage('Production: Star Service') {
			// 	agent any
			// 	steps {
			// 		startServiceOnEC2Instance('production-agent', "${production_EC2_machine_ip}", "$service_script_name")
			// 	}
			// }

			// stage('Production: Service Smoke Tests') {
			// 	agent any
			// 	steps {
			// 		echo "Smoke Tests Stage"
			// 		// clientServiceSmokeTests('production-agent', "${production_EC2_machine_ip}", "$service_name", "$deploy_version")
			// 	}
			// }

			// stage('Production: Smoke test Failed - Roll Back Version') {
			// 	agent any
			// 	when {
			// 		expression {
			// 			currentBuild.result == null || currentBuild.result == 'FAILURE'
			// 		}
			// 	}
			// 	steps {
			// 		// clientServiceRollBackVersion('production-agent', "${production_EC2_machine_ip}", "$service_name", "$deploy_version", "$service_script_name")
			// 		echo "Check Smoke Tests Stage, there were a problem with your Last Versioned Artifact"
			// 	}
			// }

			// stage('Production: Smoke test Successed') {
			// 	agent any
			// 	when {
			// 	expression {
			// 		currentBuild.result == null || currentBuild.result == 'SUCCESS'
			// 	}
			// 	}
			// 	steps {
			// 			echo "Seccessful Project!"
			// 	}
			// }

		}

	} 

