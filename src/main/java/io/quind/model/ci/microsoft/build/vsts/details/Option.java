package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Option {

    private boolean enabled;
    private OptionDefinition definition;
    private OptionInputs inputs;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public OptionDefinition getDefinition() {
        return definition;
    }

    public void setDefinition(OptionDefinition definition) {
        this.definition = definition;
    }

    public OptionInputs getInputs() {
        return inputs;
    }

    public void setInputs(OptionInputs inputs) {
        this.inputs = inputs;
    }
}
