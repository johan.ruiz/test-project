package io.quind.client.ci.helpers;

import io.quind.model.quind.jobs.JobDefinition;

import java.util.List;

public class JobDefinitionHelper {

    public static List<JobDefinition> setReleaseDetailsUrl(List<JobDefinition> buildJobDefs, List<JobDefinition> releaseJobDefs) {
        String urlReleaseDetails = "";
        for (JobDefinition buildJob : buildJobDefs) {
            if (buildJob.getUrlForDetails() != null) {
                String url = buildJob.getUrlForDetails();
                urlReleaseDetails = url.split("/_apis")[0];
                break;
            }
        }
        for (JobDefinition releaseJob : releaseJobDefs) {
            releaseJob.setUrlForDetails(urlReleaseDetails + "/_apis/release/Definitions/" + releaseJob.getId());
        }
        return releaseJobDefs;
    }
}
