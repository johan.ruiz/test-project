package io.quind.model.ci.microsoft.build.tfs.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quind.model.ci.microsoft.build.tfs.AuthoredBy;
import io.quind.model.ci.microsoft.build.tfs.Project;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildDetails {

    private AuthoredBy authoredBy;
    private String buildNumberFormat;
    private String jobAuthorizationScope;
    private int jobTimeoutInMinutes;
    private String quality;
    private int id;
    private String uri;
    private String type;
    private int revision;
    private String name;
    private String url;
    private Variables variables;
    private List<RetentionRule> retentionRules;
    private Repository repository;
    private Project project;
    private List<Option> options;
    private List<Step> build;

    public AuthoredBy getAuthoredBy() {
        return authoredBy;
    }

    public void setAuthoredBy(AuthoredBy authoredBy) {
        this.authoredBy = authoredBy;
    }

    public String getBuildNumberFormat() {
        return buildNumberFormat;
    }

    public void setBuildNumberFormat(String buildNumberFormat) {
        this.buildNumberFormat = buildNumberFormat;
    }

    public String getJobAuthorizationScope() {
        return jobAuthorizationScope;
    }

    public void setJobAuthorizationScope(String jobAuthorizationScope) {
        this.jobAuthorizationScope = jobAuthorizationScope;
    }

    public int getJobTimeoutInMinutes() {
        return jobTimeoutInMinutes;
    }

    public void setJobTimeoutInMinutes(int jobTimeoutInMinutes) {
        this.jobTimeoutInMinutes = jobTimeoutInMinutes;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Variables getVariables() {
        return variables;
    }

    public void setVariables(Variables variables) {
        this.variables = variables;
    }

    public List<RetentionRule> getRetentionRules() {
        return retentionRules;
    }

    public void setRetentionRules(List<RetentionRule> retentionRules) {
        this.retentionRules = retentionRules;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public List<Step> getBuild() {
        return build;
    }

    public void setBuild(List<Step> build) {
        this.build = build;
    }
}
