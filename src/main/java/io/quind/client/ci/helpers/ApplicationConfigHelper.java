package io.quind.client.ci.helpers;

import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class ApplicationConfigHelper {
    public static Map<String, List<ApplicationConfig>> groupAppConfigByServer(List<ApplicationConfig> applicationConfigs) {
        Map<String, List<ApplicationConfig>> configByServer = new HashMap<>();
        for (ApplicationConfig applicationConfig : applicationConfigs) {
            if (null == configByServer.get(applicationConfig.getRemoteServerIdentifier())) {
                configByServer.put(applicationConfig.getRemoteServerIdentifier(), new ArrayList<>());
            }
            configByServer.get(applicationConfig.getRemoteServerIdentifier()).add(applicationConfig);
        }
        return configByServer;
    }

    public static Map<String, List<JobDefinition>> groupJobDefsByApp(List<ApplicationConfig> applicationConfigs, JobDefinition jobDefinition, List<JobDefinition> jobDefinitionsDetails) {
    	Map<String, List<JobDefinition>> jobDefsByApp = new HashMap<>();
        for (ApplicationConfig applicationConfig : applicationConfigs) {
            if (jobDefsByApp.get(applicationConfig.getId()) == null) {
                jobDefsByApp.put(applicationConfig.getId(), new ArrayList<JobDefinition>());
            }
            if ((jobDefinitionsDetails == null || jobDefinitionsDetails.size() == 0) && ApplicationConfigHelper.isJobNameFromApplication(jobDefinition, applicationConfig)) {
                jobDefsByApp.get(applicationConfig.getId()).add(jobDefinition);
            }
            for (JobDefinition jobDefinitionTmp : jobDefinitionsDetails) {
                if (ApplicationConfigHelper.isJobNameFromApplication(jobDefinitionTmp, applicationConfig)) {
                    jobDefsByApp.get(applicationConfig.getId()).add(jobDefinitionTmp);
                }
            }
        }
        return jobDefsByApp;
    }

    static boolean isJobNameFromApplication(JobDefinition jobDefinition, ApplicationConfig applicationConfig) {
        try {
            if (applicationConfig.getAppClassifierExpression() == null || "".equals(applicationConfig.getAppClassifierExpression())) {
                return true;
            } else {
                String[] clasifiersApplication = applicationConfig.getAppClassifierExpression().split(";");
                for (String clasifier : clasifiersApplication) {
                    if (jobDefinition.getName().matches(clasifier)) {
                        return true;
                    }
                }
            }
        } catch(Exception e) {
            System.out.println("Error with ClassifierExpression in " + applicationConfig.getId());
        }

        return false;
    }

    public static String getJobPath(String urlNewJob) throws URISyntaxException {
        URI uriJob = new URI(urlNewJob);
        return uriJob.getPath();
    }

    public static String getServerHost(String urlBuildServer) throws URISyntaxException {
        URI uri = new URI(urlBuildServer);
        if(!"".equals(uri.getPath())) {
            urlBuildServer = uri.getScheme() + "://" + uri.getHost();
        }
        if(uri.getPort() != 0 && uri.getPort() != -1) {
            urlBuildServer = urlBuildServer + ":" + uri.getPort();
        }
        return urlBuildServer;
    }

    public static void setApplicationJobDefinitions(List<Application> applications, Map<String, List<JobDefinition>> jobDefsByApp) {
        for (String keyApplicationId : jobDefsByApp.keySet()) {
            Application application = new Application();
            application.setId(keyApplicationId);
            application.setJobDefinitions(jobDefsByApp.get(keyApplicationId));
            applications.add(application);
        }
    }

    public static void buildJobDefs(Map<String, List<JobDefinition>> jobDefsByApp, List<ApplicationConfig> appsConfig, List<JobDefinition> jobDefinitionsDetails, JobDefinition jobDefinition) {
        Map<String, List<JobDefinition>> jobDefsByAppTemp = groupJobDefsByApp(appsConfig, jobDefinition, jobDefinitionsDetails);
        for (String keyTemp : jobDefsByAppTemp.keySet()) {
            if (jobDefsByApp.get(keyTemp) == null) {
                jobDefsByApp.put(keyTemp, jobDefsByAppTemp.get(keyTemp));
            } else {
                jobDefsByApp.get(keyTemp).addAll(jobDefsByAppTemp.get(keyTemp));
            }
        }
    }
}
