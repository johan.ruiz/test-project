package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryProperties {

    private String labelSources;
    private String cleanOptions;
    private boolean labelSourcesFormat;
    private boolean reportBuildStatus;
    private boolean gitLfsSupport;
    private boolean skipSyncSource;
    private boolean checkoutNestedSubmodules;
    private String fetchDepth;

    public String getCleanOptions() {
        return cleanOptions;
    }

    public void setCleanOptions(String cleanOptions) {
        this.cleanOptions = cleanOptions;
    }

    public boolean isLabelSourcesFormat() {
        return labelSourcesFormat;
    }

    public void setLabelSourcesFormat(boolean labelSourcesFormat) {
        this.labelSourcesFormat = labelSourcesFormat;
    }

    public boolean isReportBuildStatus() {
        return reportBuildStatus;
    }

    public void setReportBuildStatus(boolean reportBuildStatus) {
        this.reportBuildStatus = reportBuildStatus;
    }

    public boolean isGitLfsSupport() {
        return gitLfsSupport;
    }

    public void setGitLfsSupport(boolean gitLfsSupport) {
        this.gitLfsSupport = gitLfsSupport;
    }

    public boolean isSkipSyncSource() {
        return skipSyncSource;
    }

    public void setSkipSyncSource(boolean skipSyncSource) {
        this.skipSyncSource = skipSyncSource;
    }

    public boolean isCheckoutNestedSubmodules() {
        return checkoutNestedSubmodules;
    }

    public void setCheckoutNestedSubmodules(boolean checkoutNestedSubmodules) {
        this.checkoutNestedSubmodules = checkoutNestedSubmodules;
    }

    public String getFetchDepth() {
        return fetchDepth;
    }

    public void setFetchDepth(String fetchDepth) {
        this.fetchDepth = fetchDepth;
    }

    public String getLabelSources() {
        return labelSources;
    }

    public void setLabelSources(String labelSources) {
        this.labelSources = labelSources;
    }
}
