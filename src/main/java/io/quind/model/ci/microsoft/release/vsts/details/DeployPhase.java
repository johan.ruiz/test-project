package io.quind.model.ci.microsoft.release.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeployPhase {

    private Integer rank;
    private String phaseType;
    private String name;
    private List<WorkflowTask> workflowTasks = null;

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getPhaseType() {
        return phaseType;
    }

    public void setPhaseType(String phaseType) {
        this.phaseType = phaseType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WorkflowTask> getWorkflowTasks() {
        return workflowTasks;
    }

    public void setWorkflowTasks(List<WorkflowTask> workflowTasks) {
        this.workflowTasks = workflowTasks;
    }

}
