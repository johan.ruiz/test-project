package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessParameters {

    private List<ProcessParametersInputs> inputs;

    public List<ProcessParametersInputs> getInputs() {
        return inputs;
    }

    public void setInputs(List<ProcessParametersInputs> inputs) {
        this.inputs = inputs;
    }
}
