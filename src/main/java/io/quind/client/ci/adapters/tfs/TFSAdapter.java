package io.quind.client.ci.adapters.tfs;

import io.quind.client.ci.adapters.CIAdapter;
import io.quind.model.ci.microsoft.build.tfs.Build;
import io.quind.model.ci.microsoft.build.tfs.Value;
import io.quind.model.ci.microsoft.build.tfs.details.BuildDetails;
import io.quind.model.ci.microsoft.build.tfs.details.Step;
import io.quind.model.ci.microsoft.release.tfs.Release;
import io.quind.model.ci.microsoft.release.tfs.details.DeployStep;
import io.quind.model.ci.microsoft.release.tfs.details.Environment;
import io.quind.model.ci.microsoft.release.tfs.details.ReleaseDetails;
import io.quind.model.ci.microsoft.release.tfs.details.Task;
import io.quind.model.quind.jobs.JobDefinition;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class TFSAdapter extends CIAdapter {

    public List<JobDefinition> parseBuildDefinitions(Response buildResponse) {
        Build build = new Build();
    	try {
    		build = buildResponse.readEntity(Build.class);	
    	} catch(Exception e) {
    		System.out.println(buildResponse.toString());
    	}

        List<JobDefinition> jobs = new ArrayList<>();
    	if(build != null) {
            List<Value> tfsValues = build.getValue();
            if(tfsValues != null) {
                for (Value value : tfsValues) {
                    JobDefinition job = new JobDefinition();
                    job.setUrlForDetails(value.getUrl());
                    job.setId(String.valueOf(value.getId()));
                    job.setName(value.getName());
                    job.setType(value.getType());
                    jobs.add(job);
                }
            }
        }
        return jobs;
    }

    public List<JobDefinition> parseReleaseDefinitions(Response releaseResponse) {
        Release release = new Release();
    	try {
    		release = releaseResponse.readEntity(Release.class);
    	} catch(Exception e) {
    		System.out.println(releaseResponse.toString());
    	}
        List<JobDefinition> jobs = new ArrayList<>();
        if(release != null) {
            List<io.quind.model.ci.microsoft.release.tfs.Value> tfsValues = release.getValue();
            if (tfsValues != null) {
                for (io.quind.model.ci.microsoft.release.tfs.Value value : tfsValues) {
                    JobDefinition job = new JobDefinition();
                    job.setUrlForDetails(value.getUrl());
                    job.setId(String.valueOf(value.getId()));
                    job.setName(value.getName());
                    job.setType("release");
                    jobs.add(job);
                }
            }
        }
        return jobs;
    }

    public List<JobDefinition> parseBuildDetailsDefinitions(JobDefinition jobDefinitionBase, Response buildDetailsResponse) {
        BuildDetails buildDetails = new BuildDetails();
    	try {
    		buildDetails = buildDetailsResponse.readEntity(BuildDetails.class);
    	} catch(Exception e) {
    		System.out.println(buildDetailsResponse.toString());
    	}        
        
        List<JobDefinition> jobDefinitions = new ArrayList<>();

        if (buildDetails != null && buildDetails.getBuild() != null) {
            for (Step step : buildDetails.getBuild()) {
                JobDefinition jobDefinition = new JobDefinition();
                jobDefinition.setId(jobDefinitionBase.getId());
                jobDefinition.setName(jobDefinitionBase.getName() + "->" + step.getDisplayName());
                jobDefinition.setType(jobDefinitionBase.getType());
                jobDefinitions.add(jobDefinition);
            }
        }
        return jobDefinitions;
    }

    public List<JobDefinition> parseReleaseDetailsDefinitions(JobDefinition jobDefinitionBase, Response releaseDetailsResponse) {
        ReleaseDetails releaseDetails = new ReleaseDetails();
    	try {
    		releaseDetails = releaseDetailsResponse.readEntity(ReleaseDetails.class);
    	} catch(Exception e) {
    		System.out.println(releaseDetailsResponse.toString());
    	}
        
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        List<Environment> tfsEnvironments = releaseDetails.getEnvironments();
        if (tfsEnvironments != null) {
            for (Environment environment : tfsEnvironments) {
                DeployStep tfDeploymentStep = environment.getDeployStep();
                if (tfDeploymentStep != null) {
                    List<Task> tasks = tfDeploymentStep.getTasks();
                    if (tasks != null) {
                        for (Task task : tasks) {
                            JobDefinition jobDefinition = new JobDefinition();
                            jobDefinition.setId(jobDefinitionBase.getId());
                            jobDefinition.setName(jobDefinitionBase.getName() + "->" + environment.getName() + "->" + task.getName());
                            jobDefinition.setType(jobDefinitionBase.getType());
                            jobDefinitions.add(jobDefinition);
                        }
                    }
                }
            }
        }
        return jobDefinitions;
    }

    @Override
    public List<JobDefinition> parseYamlDefinitions(JobDefinition jobDefinitionBase, Response responseYamlFile) {
        return null;
    }
}
