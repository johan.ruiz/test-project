package io.quind.client.ci.adapters.vsts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.quind.client.ci.adapters.CIAdapter;
import io.quind.model.ci.microsoft.build.vsts.Build;
import io.quind.model.ci.microsoft.build.vsts.Value;
import io.quind.model.ci.microsoft.build.vsts.details.BuildDetails;
import io.quind.model.ci.microsoft.build.vsts.details.Phase;
import io.quind.model.ci.microsoft.build.vsts.details.Step;
import io.quind.model.ci.microsoft.build.vsts.yaml.BuildProcess;
import io.quind.model.ci.microsoft.build.vsts.yaml.BuildTask;
import io.quind.model.ci.microsoft.release.vsts.Release;
import io.quind.model.ci.microsoft.release.vsts.details.DeployPhase;
import io.quind.model.ci.microsoft.release.vsts.details.Environment;
import io.quind.model.ci.microsoft.release.vsts.details.ReleaseDetails;
import io.quind.model.ci.microsoft.release.vsts.details.WorkflowTask;
import io.quind.model.quind.jobs.JobDefinition;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class VSTSAdapter extends CIAdapter {

    public List<JobDefinition> parseBuildDefinitions(Response buildResponse) {
        Build build = new Build();
        try {
        	build = buildResponse.readEntity(Build.class);
        } catch(Exception e) {
        	System.out.println(buildResponse.toString());
        }

        List<JobDefinition> jobs = new ArrayList<>();
        if(build != null) {
            List<Value> tfsValues = build.getValue();
            if(tfsValues != null) {
                for (Value value : tfsValues) {
                    JobDefinition job = new JobDefinition();
                    job.setUrlForDetails(value.getUrl());
                    job.setId(String.valueOf(value.getId()));
                    job.setName(value.getName());
                    job.setType(value.getType());
                    jobs.add(job);
                }
            }
        }
        return jobs;
    }

    public List<JobDefinition> parseReleaseDefinitions(Response releaseResponse) {
        Release release = new Release();
        try {
        	release = releaseResponse.readEntity(Release.class);
        } catch(Exception e) {
        	System.out.println(releaseResponse.toString());
        }
        List<JobDefinition> jobs = new ArrayList<>();
        if(release != null) {
            List<io.quind.model.ci.microsoft.release.vsts.Value> tfsValues = release.getValue();
            if(tfsValues != null) {
                for (io.quind.model.ci.microsoft.release.vsts.Value value : tfsValues) {
                    JobDefinition job = new JobDefinition();
                    job.setUrlForDetails(value.getUrl());
                    job.setId(String.valueOf(value.getId()));
                    job.setName(value.getName());
                    job.setType("release");
                    jobs.add(job);
                }
            }
        }

        return jobs;
    }

    public List<JobDefinition> parseBuildDetailsDefinitions(JobDefinition jobDefinitionBase, Response buildDetailsResponse) {
        BuildDetails buildDetails = new BuildDetails();
        try {
        	buildDetails = buildDetailsResponse.readEntity(BuildDetails.class);
        } catch(Exception e) {
        	System.out.println(buildDetailsResponse.toString());
        }
        
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        if (buildDetails != null && buildDetails.getProcess() != null) {
            if(buildDetails.getProcess().getYamlFilename() != null && !"".equals(buildDetails.getProcess().getYamlFilename())) {
                JobDefinition jobDefinition = new JobDefinition();
                jobDefinition.setId(jobDefinitionBase.getId());
                jobDefinition.setType(jobDefinitionBase.getType());
                jobDefinition.setYaml(true);
                jobDefinition.setYamlFileName(buildDetails.getProcess().getYamlFilename());
                jobDefinition.setRepository(buildDetails.getRepository().getName());
                jobDefinition.setBranch(buildDetails.getRepository().getDefaultBranch());
                jobDefinition.setUrlForDetails(buildDetails.getUrl());
                jobDefinitions.add(jobDefinition);
            }
            List<Phase> tfsPhases = buildDetails.getProcess().getPhases();
            if (tfsPhases != null) {
                for (Phase phase : tfsPhases) {
                    List<Step> steps = phase.getSteps();
                    if (steps != null) {
                        for (Step step : steps) {
                            JobDefinition jobDefinition = new JobDefinition();
                            jobDefinition.setId(jobDefinitionBase.getId());
                            jobDefinition.setName(jobDefinitionBase.getName() + "->" + phase.getName() + "->" + step.getDisplayName());
                            jobDefinition.setType(jobDefinitionBase.getType());
                            jobDefinitions.add(jobDefinition);
                        }
                    }
                }
            }

        }
        return jobDefinitions;
    }

    public List<JobDefinition> parseReleaseDetailsDefinitions(JobDefinition jobDefinitionBase, Response releaseDetailsResponse) {
        ReleaseDetails releaseDetails = new ReleaseDetails();
        try {
        	releaseDetails = releaseDetailsResponse.readEntity(ReleaseDetails.class);
        } catch(Exception e) {
        	System.out.println(releaseDetailsResponse.toString());
        }
        
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        List<Environment> tfsEnvironments = releaseDetails.getEnvironments();
        if (tfsEnvironments != null) {
            for (Environment environment : tfsEnvironments) {
                List<DeployPhase> tfDeploymentPhases = environment.getDeployPhases();
                if (tfDeploymentPhases != null) {
                    for (DeployPhase phase : tfDeploymentPhases) {
                        List<WorkflowTask> tasks = phase.getWorkflowTasks();
                        if (tasks != null) {
                            for (WorkflowTask task : tasks) {
                                JobDefinition jobDefinition = new JobDefinition();
                                jobDefinition.setId(jobDefinitionBase.getId());
                                jobDefinition.setName(jobDefinitionBase.getName() + "->" + environment.getName() + "->" + phase.getName() + "->" + task.getName());
                                jobDefinition.setType(jobDefinitionBase.getType());
                                jobDefinitions.add(jobDefinition);
                            }
                        }
                    }
                }
            }
        }
        return jobDefinitions;
    }

    @Override
    public List<JobDefinition> parseYamlDefinitions(JobDefinition jobDefinitionBase, Response responseYamlFile) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        BuildProcess buildProcess = null;
        try {
            buildProcess = mapper.readValue((InputStream) responseYamlFile.getEntity(), BuildProcess.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        List<BuildTask> tasks = buildProcess.getSteps();
        if (tasks != null) {
            for (BuildTask task : tasks) {
                JobDefinition jobDefinition = new JobDefinition();
                jobDefinition.setId(jobDefinitionBase.getId());
                if(task.getDisplayName() != null && !"".equals(task.getDisplayName())) {
                    jobDefinition.setName(jobDefinitionBase.getName() + "->" + task.getDisplayName());
                } else {
                    jobDefinition.setName(jobDefinitionBase.getName() + "->" + task.getTask());
                }
                jobDefinition.setType(jobDefinitionBase.getType());
                jobDefinitions.add(jobDefinition);
            }
        }
        return jobDefinitions;
    }
}
