package io.quind.model.ci.microsoft.release.tfs.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Environment {

    private Integer id;
    private String name;
    private Integer rank;
    private DeployStep deployStep = null;
    private List<Object> demands = null;
    private List<Object> schedules = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public DeployStep getDeployStep() {
        return deployStep;
    }

    public void setDeployStep(DeployStep deployStep) {
        this.deployStep = deployStep;
    }

    public List<Object> getDemands() {
        return demands;
    }

    public void setDemands(List<Object> demands) {
        this.demands = demands;
    }

    public List<Object> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Object> schedules) {
        this.schedules = schedules;
    }


}
