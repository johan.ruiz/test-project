package io.quind.client.ci.facade;

import io.quind.client.helpers.ConstantDataManager;
import io.quind.client.facade.ClientWorkerThread;
import io.quind.repository.QuindRepository;
import io.quind.rest.RestClient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ClientAgent {

    public static void main(String[] args) {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
        ExecutorService executor = Executors.newFixedThreadPool(10);
        RestClient restClient = new RestClient();
        ClientFacade clientFacade = new ClientFacade(restClient, new QuindRepository(restClient, ConstantDataManager.QUIND_SERVER()));
        scheduledThreadPool.scheduleAtFixedRate(new ClientWorkerThread(executor, clientFacade), 0, ConstantDataManager.FRECUENCIA_EJECUCION_SEGUNDOS(), TimeUnit.SECONDS);
    }

}
