package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quind.model.ci.microsoft.build.tfs.details.BuildConfiguration;
import io.quind.model.ci.microsoft.build.tfs.details.BuildPlatform;
import io.quind.model.ci.microsoft.build.tfs.details.SystemDebug;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Variables {

    private BuildConfiguration buildConfiguration;
    private BuildPlatform buildPlatform;
    private SystemDebug systemDebug;

    public BuildConfiguration getBuildConfiguration() {
        return buildConfiguration;
    }

    public void setBuildConfiguration(BuildConfiguration buildConfiguration) {
        this.buildConfiguration = buildConfiguration;
    }

    public BuildPlatform getBuildPlatform() {
        return buildPlatform;
    }

    public void setBuildPlatform(BuildPlatform buildPlatform) {
        this.buildPlatform = buildPlatform;
    }

    public SystemDebug getSystemDebug() {
        return systemDebug;
    }

    public void setSystemDebug(SystemDebug systemDebug) {
        this.systemDebug = systemDebug;
    }
}
