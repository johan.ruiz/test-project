package io.quind.client.ci.repository.tfs;

import io.quind.client.ci.adapters.CIAdapter;
import io.quind.client.ci.helpers.JobDefinitionHelper;
import io.quind.client.ci.repository.CIRepository;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;
import io.quind.rest.RestClient;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class TFSRepository extends CIRepository {

    public TFSRepository(RestClient restClient, CIAdapter ciAdapter) {
        super(restClient, ciAdapter);
    }

    @Override
    public List<JobDefinition> getJobDefinitions(ApplicationConfig applicationConfig) {
        List<JobDefinition> jobsBuild = getBuildJobDefinitions(applicationConfig);
        List<JobDefinition> jobsRelease = getReleaseJobDefinitions(applicationConfig);
        List<JobDefinition> jobsReleaseWithUrl = JobDefinitionHelper.setReleaseDetailsUrl(jobsBuild, jobsRelease);
        List<JobDefinition> jobs = new ArrayList<>();
        jobs.addAll(jobsBuild);
        jobs.addAll(jobsReleaseWithUrl);
        return jobs;
    }
}
