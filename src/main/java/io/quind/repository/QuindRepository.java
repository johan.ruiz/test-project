package io.quind.repository;

import io.quind.client.helpers.ConstantDataManager;
import io.quind.model.exceptions.QuindException;
import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.rest.RestClient;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

public class QuindRepository {

    private RestClient restClient;
    private String quindUrl;

    public QuindRepository(RestClient restClient, String quindUrl) {
        this.restClient = restClient;
        this.quindUrl = quindUrl;
    }

    @SuppressWarnings("unchecked")
	public List<Integer> getApiClients(String token) {
    	ArrayList<Integer> apiClients = new ArrayList<>();
    	if(ConstantDataManager.USE_PROXY()) {
    		apiClients = restClient.doGet(this.quindUrl + "/api/v1/api-clients/" + token + "/roots", ConstantDataManager.PROXY_SERVER(), ConstantDataManager.PROXY_USERNAME(), ConstantDataManager.PROXY_PASSWORD()).readEntity(ArrayList.class);
            if (apiClients.isEmpty() || apiClients == null) {
                throw new QuindException("EL servicio de consulta de APi Clients no devolvio informacion");
            }	
    	} else {
    		apiClients = restClient.doGet(this.quindUrl + "/api/v1/api-clients/" + token + "/roots").readEntity(ArrayList.class);
            if (apiClients.isEmpty() || apiClients == null) {
                throw new QuindException("EL servicio de consulta de APi Clients no devolvio informacion");
            }	
    	}
        return apiClients;
    }

    public boolean enviarJobDefinitions(List<Application> applications) {
        boolean result = true;
        if(ConstantDataManager.USE_PROXY()) {
            for (Application application : applications) {
	            int response = restClient.doPost(this.quindUrl + "/api/v1/components/" + application.getId() + "/jobs", application.getJobDefinitions(), ConstantDataManager.PROXY_SERVER(), ConstantDataManager.PROXY_USERNAME(), ConstantDataManager.PROXY_PASSWORD()).getStatus();
	            result = result && (response == 201);
	        }
    	} else {
	        for (Application application : applications) {
	            int response = restClient.doPost(this.quindUrl + "/api/v1/components/" + application.getId() + "/jobs", application.getJobDefinitions()).getStatus();
	            result = result && (response == 201);
	        }
    	}
        return result;
    }

    public boolean finishProcess(int apiClient) {
        int response = 0;
        if(ConstantDataManager.USE_PROXY()) {
        	response = restClient.doPost(this.quindUrl + "/api/v1/components/" + apiClient + "/run-status", "", ConstantDataManager.PROXY_SERVER(), ConstantDataManager.PROXY_USERNAME(), ConstantDataManager.PROXY_PASSWORD()).getStatus();
    	} else {		
    		response = restClient.doPost(this.quindUrl + "/api/v1/components/" + apiClient + "/run-status", "").getStatus();
    	}
        
    	if (response == 200) {
            return true;
        } else {
            return false;
        }
    }


    public List<ApplicationConfig> getProviderConfiguration(int idProveedorInformacion) {
    	List<ApplicationConfig> applicationConfigs = new ArrayList<>();
    	Response response = null;
    	
    	if(ConstantDataManager.USE_PROXY()) {
    		response = restClient.doGet(this.quindUrl + "/api/v1/components/" + idProveedorInformacion + "/leaves", ConstantDataManager.PROXY_SERVER(), ConstantDataManager.PROXY_USERNAME(), ConstantDataManager.PROXY_PASSWORD());
    	} else {
    		response = restClient.doGet(this.quindUrl + "/api/v1/components/" + idProveedorInformacion + "/leaves");
    	}
    	
        try {
        	applicationConfigs = response.readEntity(new GenericType<List<ApplicationConfig>>() {});	
        } catch(Exception e) {
        	System.out.println(response.toString());
        }
    	
        return applicationConfigs;
    }
}