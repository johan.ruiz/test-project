package io.quind.model.quind.servers;

public enum TypeOfBuildServer {

    VSTS(1), TFS(2), JENKINS(3);

    private int code;

    TypeOfBuildServer(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
