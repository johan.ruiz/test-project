package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OptionInputs {

    private String branchFilters;
    private String additionalFields;
    private String workItemType;
    private boolean assignToRequestor;

    public boolean isAssignToRequestor() {
        return assignToRequestor;
    }

    public void setAssignToRequestor(boolean assignToRequestor) {
        this.assignToRequestor = assignToRequestor;
    }

    public String getWorkItemType() {
        return workItemType;
    }

    public void setWorkItemType(String workItemType) {
        this.workItemType = workItemType;
    }

    public String getBranchFilters() {

        return branchFilters;
    }

    public void setBranchFilters(String branchFilters) {
        this.branchFilters = branchFilters;
    }

    public String getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(String additionalFields) {
        this.additionalFields = additionalFields;
    }
}
