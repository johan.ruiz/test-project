package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Process {

    private List<Phase> phases = null;
    private Integer type;
    private String yamlFilename;

    public List<Phase> getPhases() {
        return phases;
    }

    public void setPhases(List<Phase> phases) {
        this.phases = phases;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getYamlFilename() {
        return yamlFilename;
    }

    public void setYamlFilename(String yamlFilename) {
        this.yamlFilename = yamlFilename;
    }
}
