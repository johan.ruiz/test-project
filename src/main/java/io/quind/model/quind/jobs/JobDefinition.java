package io.quind.model.quind.jobs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobDefinition {

    private String id;
    private String name;
    private String type;
    private String urlForDetails;
    private List<Build> builds;
    private boolean isYaml;
    private String yamlFileName;
    private String repository;
    private String branch;

    public JobDefinition() {
        super();
    }

    public JobDefinition(String id, String name, String type, String urlForDetails, List<Build> builds) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.urlForDetails = urlForDetails;
        this.builds = builds;
    }

    public JobDefinition(String id, String name, String type, String urlForDetails) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.urlForDetails = urlForDetails;
    }

    public JobDefinition(String id, String name, String type, String urlForDetails, List<Build> builds, boolean isYaml, String yamlFileName, String repository, String branch) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.urlForDetails = urlForDetails;
        this.builds = builds;
        this.isYaml = isYaml;
        this.yamlFileName = yamlFileName;
        this.repository = repository;
        this.branch = branch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrlForDetails() {
        return urlForDetails;
    }

    public void setUrlForDetails(String urlForDetails) {
        this.urlForDetails = urlForDetails;
    }

    public List<Build> getBuilds() {
        return builds;
    }

    public void setBuilds(List<Build> builds) {
        this.builds = builds;
    }

    public boolean isYaml() {
        return isYaml;
    }

    public void setYaml(boolean yaml) {
        isYaml = yaml;
    }

    public String getYamlFileName() {
        return yamlFileName;
    }

    public void setYamlFileName(String yamlFileName) {
        this.yamlFileName = yamlFileName;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
