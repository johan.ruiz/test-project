package io.quind.model.ci.microsoft.build.tfs.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryProperties {

    private String labelSources;

    public String getLabelSources() {
        return labelSources;
    }

    public void setLabelSources(String labelSources) {
        this.labelSources = labelSources;
    }
}
