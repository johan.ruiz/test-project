package io.quind.client.ci.helpers;

import io.quind.model.quind.jobs.JobDefinition;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class JobDefinitionHelperTest {

    @Test
    public void shouldSetReleaseDetailsUrl() {
        List<JobDefinition> buildJobs = Arrays.asList(new JobDefinition("1", "JenkinsFolder JenkinsFolderJob 1",
                "build", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/build/Definitions/1"));
        List<JobDefinition> releaseJobs = Arrays.asList(new JobDefinition("2", "Release JenkinsFolderJob 2", "release", ""));
        List<JobDefinition> expectedReleaseJobs = Arrays.asList(new JobDefinition("2", "Release JenkinsFolderJob 2",
                "release", "http://vmhera:8080/tfs/Transversales/78cc984b-856d-4fd1-b7c6-b9ed9a2fd68d/_apis/release/Definitions/2"));
        List<JobDefinition> result = JobDefinitionHelper.setReleaseDetailsUrl(buildJobs, releaseJobs);
        assertEquals(expectedReleaseJobs.get(0).getId(), result.get(0).getId());
        assertEquals(expectedReleaseJobs.get(0).getName(), result.get(0).getName());
        assertEquals(expectedReleaseJobs.get(0).getType(), result.get(0).getType());
        assertEquals(expectedReleaseJobs.get(0).getUrlForDetails(), result.get(0).getUrlForDetails());
    }
}
