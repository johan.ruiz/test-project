package io.quind.client.ci.repository.vsts;

import io.quind.client.ci.adapters.CIAdapter;
import io.quind.client.ci.repository.CIRepository;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;
import io.quind.rest.RestClient;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class VSTSRepository extends CIRepository {

    public VSTSRepository(RestClient restClient, CIAdapter ciAdapter) {
        super(restClient, ciAdapter);
    }

    @Override
    public List<JobDefinition> getJobDefinitions(ApplicationConfig applicationConfig) {
        List<JobDefinition> jobsBuild = getBuildJobDefinitions(applicationConfig);
        List<JobDefinition> jobsRelease = getReleaseJobDefinitions(applicationConfig);
        List<JobDefinition> jobs = new ArrayList<>();
        jobs.addAll(jobsBuild);
        jobs.addAll(jobsRelease);
        return jobs;
    }
}
