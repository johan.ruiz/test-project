package io.quind.client.ci.repository.jenkins;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.quind.client.ci.adapters.jenkins.JenkinsAdapter;
import io.quind.rest.RestClient;

import javax.ws.rs.core.*;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class JenkinsRepositoryTest {

	@Mock
	RestClient restClient;

	JenkinsAdapter ciAdapter = new JenkinsAdapter();
	JenkinsRepository jenkinsRepository;

	public JenkinsRepositoryTest() {
		MockitoAnnotations.initMocks(this);
		jenkinsRepository = new JenkinsRepository(restClient, ciAdapter);
	}

    //TODO Fix this shit
	/*
	@SuppressWarnings("unchecked")
	@Test
	public void shouldGetJobDefinitions() {
		Action action = new Action();
		action.setClass_("hudson.model.CauseAction");
		List<Action> actions = new ArrayList<>();
		actions.add(action);

		JenkinsFolderJob job = new JenkinsFolderJob();
		job.setClass_("hudson.model.FreeStyleProject");
		List<JenkinsFolderJob> jobs = new ArrayList<>();
		jobs.add(job);

		JenkinsFolder jenkinsJob = new JenkinsFolder();
		jenkinsJob.setClass_("com.cloudbees.hudson.plugins.folder.Folder");
		jenkinsJob.setJobs(jobs);

		final Response buildResponse = Mockito.mock(Response.class);
		Mockito.when(buildResponse.readEntity(Mockito.any(Class.class))).thenReturn(jenkinsJob);
		doReturn(buildResponse).when(restClient).doGet(
				"http://fakeurl.jenkins.com/fakepath/api/json",
				"username", "password");

		List<JobDefinition> expectedJobs = new ArrayList<>();
		expectedJobs.add(new JobDefinition("com.cloudbees.hudson.plugins.folder.Folder",
				"com.cloudbees.hudson.plugins.folder.Folder", "build", null));

		List<JobDefinition> jobDefinitios = jenkinsRepository
				.getJobDefinitions(new ApplicationConfig("1", "fakepath", "expression", "http://fakeurl.jenkins.com",
						"http://fakeurl.jenkins.com", 3, "username", "password", "username", "password", "Name"));
		assertEquals(expectedJobs.size(), jobDefinitios.size());
		assertEquals(expectedJobs.get(0).getType(), jobDefinitios.get(0).getType());
		assertEquals(expectedJobs.get(0).getName(), jobDefinitios.get(0).getName());

	}
	*/

    @Test
    public void pruebaMarica() {
        String jobName = "Proyectos » Advance » Advance » Definición » Análisis  » 1. Ciclo de Desarrollo » 3. Ejecutar análisis estático de código_MSAdvanceDefinicionAnalisis #185->jenkins.metrics.impl.TimeInQueueAction";
        String regex = ".*MSAdvanceDefinicionAnalisis.*|.*FUN_MSCelulaDefinicion.*";
        assertTrue(jobName.matches(regex));
    }
}
