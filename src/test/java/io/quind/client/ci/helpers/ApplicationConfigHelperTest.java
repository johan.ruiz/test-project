package io.quind.client.ci.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;

public class ApplicationConfigHelperTest {

	@Test
	public void shouldGroupAppConfigByServer() {
		ApplicationConfig applicationConfigOne = new ApplicationConfig("1", "path", "appClassifierExpression", "urlBuildServer", "urlReleaseManager", 1, "usernameBuildServer", "tokenBuildServer", "usernameReleaseManager", "tokenReleaseManager", "nameOne");
		ApplicationConfig applicationConfigTwo = new ApplicationConfig("2", "path", "appClassifierExpression", "urlBuildServer", "urlReleaseManager", 1, "usernameBuildServer", "tokenBuildServer", "usernameReleaseManager", "tokenReleaseManager", "nameTwo");
		ApplicationConfig applicationConfigThree = new ApplicationConfig("3", "path", "appClassifierExpression", "urlBuildServerTwo", "urlReleaseManager", 1, "usernameBuildServer", "tokenBuildServer", "usernameReleaseManager", "tokenReleaseManager", "nameThree");
		ApplicationConfig applicationConfigFour = new ApplicationConfig("4", "path", "appClassifierExpression", "urlBuildServerTwo", "urlReleaseManager", 1, "usernameBuildServer", "tokenBuildServer", "usernameReleaseManager", "tokenReleaseManager", "nameFour");
		List<ApplicationConfig> applicationConfigs = new ArrayList<>();
		applicationConfigs.add(applicationConfigOne);
		applicationConfigs.add(applicationConfigTwo);
		applicationConfigs.add(applicationConfigThree);
		applicationConfigs.add(applicationConfigFour);

		Map<String, List<ApplicationConfig>> result = ApplicationConfigHelper.groupAppConfigByServer(applicationConfigs);
		assertEquals(result.get("urlBuildServerurlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").size(), 2);
		assertEquals(result.get("urlBuildServerurlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").get(0).getId(), "1");
		assertEquals(result.get("urlBuildServerurlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").get(1).getId(), "2");
		
		assertEquals(result.get("urlBuildServerTwourlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").size(), 2);
		assertEquals(result.get("urlBuildServerTwourlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").get(0).getId(), "3");
		assertEquals(result.get("urlBuildServerTwourlReleaseManagerusernameBuildServerusernameReleaseManagertokenBuildServertokenReleaseManagerpath").get(1).getId(), "4");
	}
	
	@Test
	public void shouldMatchJobNameWithApplicationWithEmptyClassifiers() {
		JobDefinition jobDefinition = new JobDefinition();
		ApplicationConfig applicationConfig = new ApplicationConfig();
		
		assertTrue(ApplicationConfigHelper.isJobNameFromApplication(jobDefinition, applicationConfig));
    }
	
	@Test
	public void shouldMatchJobNameWithApplicationWithOneClassifier() {
		JobDefinition jobDefinition = new JobDefinition();
		jobDefinition.setName("test1");
		ApplicationConfig applicationConfig = new ApplicationConfig();
		applicationConfig.setAppClassifierExpression("test1");
		
		assertTrue(ApplicationConfigHelper.isJobNameFromApplication(jobDefinition, applicationConfig));
    }
	
	@Test
	public void shouldMatchJobNameWithApplicationWithSeveralClassifiers() {
		JobDefinition jobDefinition = new JobDefinition();
		jobDefinition.setName("testTwo");
		ApplicationConfig applicationConfig = new ApplicationConfig();
		applicationConfig.setAppClassifierExpression("testOne;testTwo;testThree");
		
		assertTrue(ApplicationConfigHelper.isJobNameFromApplication(jobDefinition, applicationConfig));
    }
	
	@Test
	public void shouldNotMatchJobNameWithApplication() {
		JobDefinition jobDefinition = new JobDefinition();
		jobDefinition.setName("testTwo");
		ApplicationConfig applicationConfig = new ApplicationConfig();
		applicationConfig.setAppClassifierExpression("testOne;testThree");
		
		assertFalse(ApplicationConfigHelper.isJobNameFromApplication(jobDefinition, applicationConfig));
    }

    @Test
    public void shouldFixServerUrl() {
	    String urlToFix = "http://10.65.1.118:18089/jenkins";
        String fixedUrl = "";
        try {
            fixedUrl = ApplicationConfigHelper.getServerHost(urlToFix);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals(fixedUrl, "http://10.65.1.118:18089");
    }

    @Test
    public void shouldGetNewJobPath() {
        String urlNewJob = "https://vostpmde36.proteccion.com.co/jenkins/job/PROJECTS/job/ADVANCE/job/TECNICO/job/eventos/job/PRE_PROD/";
        String jobPath = "";
        try {
            jobPath = ApplicationConfigHelper.getJobPath(urlNewJob);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals(jobPath, "/jenkins/job/PROJECTS/job/ADVANCE/job/TECNICO/job/eventos/job/PRE_PROD/");
    }
	
}
