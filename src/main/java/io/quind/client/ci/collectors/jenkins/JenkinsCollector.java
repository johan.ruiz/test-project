package io.quind.client.ci.collectors.jenkins;

import io.quind.client.ci.collectors.CICollector;
import io.quind.client.ci.helpers.ApplicationConfigHelper;
import io.quind.client.ci.repository.jenkins.JenkinsRepository;
import io.quind.model.ci.jenkins.JenkinsFolderJob;
import io.quind.model.ci.jenkins.JenkinsJobBuild;
import io.quind.model.ci.jenkins.JenkinsJobDefinition;
import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JenkinsCollector extends CICollector {

    private JenkinsRepository jenkinsRepository;
    private List<JenkinsJobDefinition> jobDetailDefinitions = new ArrayList<>();

    public JenkinsCollector(JenkinsRepository jenkinsRepository) {
        this.jenkinsRepository = jenkinsRepository;
    }

    @Override
    protected void collectJobDefinitionsByServer(Map<String, List<ApplicationConfig>> configsByServer, List<Application> applications, Map<String, List<JobDefinition>> jobDefsByApp, String key) throws URISyntaxException {
        List<ApplicationConfig> appsConfig = configsByServer.get(key);
        ApplicationConfig appConfig = appsConfig.iterator().next();
        getDetailJobDefinitions(appConfig, ApplicationConfigHelper.getServerHost(appConfig.getUrlBuildServer()));

        for(JenkinsJobDefinition jenkinsJobDefinition : jobDetailDefinitions) {
            if(jenkinsJobDefinition.getBuilds() != null && jenkinsJobDefinition.getBuilds().size() > 0) {
                JobDefinition jobDefinition = parseJobDefinition(jenkinsJobDefinition);
                JenkinsJobBuild jenkinsLastBuild = getLastBuild(jenkinsJobDefinition);

                List<JobDefinition> jobDefinitionsDetails = jenkinsRepository.getBuildDefinition(jenkinsJobDefinition, jenkinsLastBuild, appConfig);
                ApplicationConfigHelper.buildJobDefs(jobDefsByApp, appsConfig, jobDefinitionsDetails, jobDefinition);
                jobDefinitionsDetails = null;
            }
        }

        ApplicationConfigHelper.setApplicationJobDefinitions(applications, jobDefsByApp);
        jobDetailDefinitions = new ArrayList<>();
    }

    private JobDefinition parseJobDefinition(JenkinsJobDefinition jenkinsJobDefinition) {
        return new JobDefinition(null, jenkinsJobDefinition.getDisplayName(), "build", "");
    }

    private void getDetailJobDefinitions(ApplicationConfig appConfig, String hostUrl) throws URISyntaxException {
        JenkinsJobDefinition jobDefinition = jenkinsRepository.getJobDefinition(appConfig);
        if(jobDefinition != null) {
            if("com.cloudbees.hudson.plugins.folder.Folder".equals(jobDefinition.get_class()) || "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject".equals(jobDefinition.get_class())) {
                for (JenkinsFolderJob jenkinsFolderJob : jobDefinition.getJobs()) {
                    appConfig.setUrlBuildServer(hostUrl);
                    appConfig.setJobsPath(ApplicationConfigHelper.getJobPath(jenkinsFolderJob.getUrl()));
                    getDetailJobDefinitions(appConfig, hostUrl);
                }
            } else {
                jobDetailDefinitions.add(jobDefinition);
            }
        }
    }

    private JenkinsJobBuild getLastBuild(JenkinsJobDefinition jenkinsJobDefinition) {
        JenkinsJobBuild lastBuild = new JenkinsJobBuild();
        for(JenkinsJobBuild jenkinsJobBuild : jenkinsJobDefinition.getBuilds()) {
            if(jenkinsJobBuild.getNumber() > lastBuild.getNumber()) {
                lastBuild = jenkinsJobBuild;
            }
        }
        return lastBuild;
    }
}
