package io.quind.client.ci.repository;

import io.quind.client.ci.adapters.CIAdapter;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.model.quind.jobs.JobDefinition;
import io.quind.rest.RestClient;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class CIRepository {

    protected RestClient restClient;
    private CIAdapter ciAdapter;

    public CIRepository(RestClient restClient, CIAdapter ciAdapter) {
        this.restClient = restClient;
        this.ciAdapter = ciAdapter;
    }

    abstract public List<JobDefinition> getJobDefinitions(ApplicationConfig applicationConfig);

    private Response getResponse(ApplicationConfig applicationConfig, String urlBuildServer, String s, String usernameBuildServer, String tokenBuildServer) {
        Response buildDefinition = null;
        try {
            buildDefinition = restClient.doGet(urlBuildServer + "/" + applicationConfig.getJobsPath() + s, usernameBuildServer, tokenBuildServer);
        } catch (Exception e) {
            e.printStackTrace(); // El proceso debe continuar para recolectar la información de los otros servicios.
        }
        return buildDefinition;
    }

    protected List<JobDefinition> getReleaseJobDefinitions(ApplicationConfig applicationConfig) {
        Response releaseDefinition = getResponse(applicationConfig,
                applicationConfig.getUrlReleaseManager(),
                "/_apis/release/definitions",
                applicationConfig.getUsernameReleaseManager(),
                applicationConfig.getTokenReleaseManager());

        return ciAdapter.parseReleaseDefinitions(releaseDefinition);
    }

    protected List<JobDefinition> getBuildJobDefinitions(ApplicationConfig applicationConfig) {
        Response buildDefinition = getResponse(applicationConfig,
                applicationConfig.getUrlBuildServer(),
                "/_apis/build/definitions?api-version=2.0",
                applicationConfig.getUsernameBuildServer(),
                applicationConfig.getTokenBuildServer());
        return ciAdapter.parseBuildDefinitions(buildDefinition);
    }

    public List<JobDefinition> getJobDefinitionsDetails(JobDefinition jobBuildParam, ApplicationConfig applicationConfig) {
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        Response responseDefinitionsDetails = restClient.doGet(jobBuildParam.getUrlForDetails(), applicationConfig.getUsernameBuildServer(), applicationConfig.getTokenBuildServer());
        if ("build".equals(jobBuildParam.getType())) {
            jobDefinitions.addAll(ciAdapter.parseBuildDetailsDefinitions(jobBuildParam, responseDefinitionsDetails));
        } else {
            jobDefinitions.addAll(ciAdapter.parseReleaseDetailsDefinitions(jobBuildParam, responseDefinitionsDetails));
        }
        return jobDefinitions;
    }

    public List<JobDefinition> getJobDefinitionsFromYaml(JobDefinition jobDefinitionBase, JobDefinition jobDefinitionsDetail, ApplicationConfig appConfig) {
        List<JobDefinition> jobDefinitions = new ArrayList<>();
        Response responseYamlFile = restClient.doGet(appConfig.getUrlBuildServer() + "/" + appConfig.getJobsPath() +
                "/_apis/sourceProviders/tfsgit/filecontents?api-version=5.1-preview&repository=" +
                jobDefinitionsDetail.getRepository() + "&commitOrBranch=" + jobDefinitionsDetail.getBranch() +
                "&path=" + jobDefinitionsDetail.getYamlFileName(),
                appConfig.getUsernameBuildServer(),
                appConfig.getTokenBuildServer());
        return ciAdapter.parseYamlDefinitions(jobDefinitionBase, responseYamlFile);
    }
}
