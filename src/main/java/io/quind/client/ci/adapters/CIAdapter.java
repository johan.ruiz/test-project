package io.quind.client.ci.adapters;

import io.quind.model.quind.jobs.JobDefinition;

import javax.ws.rs.core.Response;
import java.util.List;

public abstract class CIAdapter {

    public abstract List<JobDefinition> parseBuildDefinitions(Response buildResponse);

    public abstract List<JobDefinition> parseReleaseDefinitions(Response releaseResponse);

    public abstract List<JobDefinition> parseBuildDetailsDefinitions(JobDefinition jobDefinitionBase, Response buildDetailsResponse);

    public abstract List<JobDefinition> parseReleaseDetailsDefinitions(JobDefinition jobDefinitionBase, Response releaseDetailsResponse);

    public abstract List<JobDefinition> parseYamlDefinitions(JobDefinition jobDefinitionBase, Response responseYamlFile);
}
