package io.quind.model.ci.microsoft.build.vsts.yaml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildTask {
    private String task;
    private String displayName;
    private Map<String, String> inputs;

    public BuildTask(String task, String displayName, Map<String, String> inputs) {
        this.task = task;
        this.displayName = displayName;
        this.inputs = inputs;
    }

    public BuildTask() {
        super();
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Map<String, String> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, String> inputs) {
        this.inputs = inputs;
    }
}
