package io.quind.model.ci.microsoft.release.tfs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetentionPolicy {

    private Integer daysToKeep;

    public Integer getDaysToKeep() {
        return daysToKeep;
    }

    public void setDaysToKeep(Integer daysToKeep) {
        this.daysToKeep = daysToKeep;
    }

}
