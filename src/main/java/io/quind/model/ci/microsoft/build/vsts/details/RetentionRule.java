package io.quind.model.ci.microsoft.build.vsts.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetentionRule {

    private List<String> branches;
    private List<String> artifacts;
    private List<String> artifactTypesToDelete;
    private int daysToKeep;
    private int minimumToKeep;
    private boolean deleteBuildRecord;
    private boolean deleteTestResults;

    public List<String> getBranches() {
        return branches;
    }

    public void setBranches(List<String> branches) {
        this.branches = branches;
    }

    public List<String> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(List<String> artifacts) {
        this.artifacts = artifacts;
    }

    public List<String> getArtifactTypesToDelete() {
        return artifactTypesToDelete;
    }

    public void setArtifactTypesToDelete(List<String> artifactTypesToDelete) {
        this.artifactTypesToDelete = artifactTypesToDelete;
    }

    public int getDaysToKeep() {
        return daysToKeep;
    }

    public void setDaysToKeep(int daysToKeep) {
        this.daysToKeep = daysToKeep;
    }

    public int getMinimumToKeep() {
        return minimumToKeep;
    }

    public void setMinimumToKeep(int minimumToKeep) {
        this.minimumToKeep = minimumToKeep;
    }

    public boolean isDeleteBuildRecord() {
        return deleteBuildRecord;
    }

    public void setDeleteBuildRecord(boolean deleteBuildRecord) {
        this.deleteBuildRecord = deleteBuildRecord;
    }

    public boolean isDeleteTestResults() {
        return deleteTestResults;
    }

    public void setDeleteTestResults(boolean deleteTestResults) {
        this.deleteTestResults = deleteTestResults;
    }
}
