package io.quind.model.ci.microsoft.build.vsts.yaml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildProcess {
    private Map<String, String> pool;
    private Map<String, String> variables;
    private List<BuildTask> steps;

    public BuildProcess(Map<String, String> pool, Map<String, String> variables, List<BuildTask> steps) {
        this.pool = pool;
        this.variables = variables;
        this.steps = steps;
    }

    public BuildProcess() {
        super();
    }

    public Map<String, String> getPool() {
        return pool;
    }

    public void setPool(Map<String, String> pool) {
        this.pool = pool;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public List<BuildTask> getSteps() {
        return steps;
    }

    public void setSteps(List<BuildTask> steps) {
        this.steps = steps;
    }
}
