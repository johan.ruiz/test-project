package io.quind.model.ci.jenkins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JenkinsJobDefinition {

    private String _class;
    private String displayName;
    private String fullDisplayName;
    private List<JenkinsJobBuild> builds;
    private List<JenkinsFolderJob> jobs = null;

    public JenkinsJobDefinition() {
    }

    public JenkinsJobDefinition(String _class, String displayName, String fullDisplayName, List<JenkinsJobBuild> builds, List<JenkinsFolderJob> jobs) {
        this._class = _class;
        this.displayName = displayName;
        this.fullDisplayName = fullDisplayName;
        this.builds = builds;
        this.jobs = jobs;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFullDisplayName() {
        return fullDisplayName;
    }

    public void setFullDisplayName(String fullDisplayName) {
        this.fullDisplayName = fullDisplayName;
    }

    public List<JenkinsJobBuild> getBuilds() {
        return builds;
    }

    public void setBuilds(List<JenkinsJobBuild> builds) {
        this.builds = builds;
    }

    public List<JenkinsFolderJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<JenkinsFolderJob> jobs) {
        this.jobs = jobs;
    }
}
