package io.quind.model.quind.applications;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationConfig {

    private String id;
    private String jobsPath;
    private String appClassifierExpression;
    private String urlBuildServer;
    private String urlReleaseManager;
    private int typeOfBuildServer;
    private String usernameBuildServer;
    private String tokenBuildServer;
    private String usernameReleaseManager;
    private String tokenReleaseManager;
    private String name;

    public ApplicationConfig() {

    }

    public ApplicationConfig(String id, String jobsPath, String appClassifierExpression, String urlBuildServer, String urlReleaseManager, int typeOfBuildServer, String usernameBuildServer, String tokenBuildServer, String usernameReleaseManager, String tokenReleaseManager, String name) {
        this.id = id;
        this.jobsPath = jobsPath;
        this.appClassifierExpression = appClassifierExpression;
        this.urlBuildServer = urlBuildServer;
        this.urlReleaseManager = urlReleaseManager;
        this.typeOfBuildServer = typeOfBuildServer;
        this.usernameBuildServer = usernameBuildServer;
        this.tokenBuildServer = tokenBuildServer;
        this.usernameReleaseManager = usernameReleaseManager;
        this.tokenReleaseManager = tokenReleaseManager;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobsPath() {
        return jobsPath;
    }

    public void setJobsPath(String jobsPath) {
        this.jobsPath = jobsPath;
    }

    public String getAppClassifierExpression() {
        return appClassifierExpression;
    }

    public void setAppClassifierExpression(String appClassifierExpression) {
        this.appClassifierExpression = appClassifierExpression;
    }

    public String getUrlBuildServer() {
        return urlBuildServer;
    }

    public void setUrlBuildServer(String urlBuildServer) {
        this.urlBuildServer = urlBuildServer;
    }

    public String getUrlReleaseManager() {
        return urlReleaseManager;
    }

    public void setUrlReleaseManager(String urlReleaseManager) {
        this.urlReleaseManager = urlReleaseManager;
    }

    public String getUsernameBuildServer() {
        return usernameBuildServer;
    }

    public void setUsernameBuildServer(String usernameBuildServer) {
        this.usernameBuildServer = usernameBuildServer;
    }

    public String getTokenBuildServer() {
        return tokenBuildServer;
    }

    public void setTokenBuildServer(String tokenBuildServer) {
        this.tokenBuildServer = tokenBuildServer;
    }

    public String getUsernameReleaseManager() {
        return usernameReleaseManager;
    }

    public void setUsernameReleaseManager(String usernameReleaseManager) {
        this.usernameReleaseManager = usernameReleaseManager;
    }

    public String getTokenReleaseManager() {
        return tokenReleaseManager;
    }

    public void setTokenReleaseManager(String tokenReleaseManager) {
        this.tokenReleaseManager = tokenReleaseManager;
    }

    public String getRemoteServerIdentifier() {
        return urlBuildServer + urlReleaseManager + usernameBuildServer + usernameReleaseManager + tokenBuildServer + tokenReleaseManager + jobsPath;
    }

    public int getTypeOfBuildServer() {
        return typeOfBuildServer;
    }

    public void setTypeOfBuildServer(int typeOfBuildServer) {
        this.typeOfBuildServer = typeOfBuildServer;
    }
}
