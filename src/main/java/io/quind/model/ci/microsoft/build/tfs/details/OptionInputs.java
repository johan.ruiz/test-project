package io.quind.model.ci.microsoft.build.tfs.details;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OptionInputs {

    private String multipliers;
    private boolean parallel;
    private boolean continueOnError;
    private String additionalFields;
    private String workItemType;
    private boolean assignToRequestor;

    public boolean isAssignToRequestor() {
        return assignToRequestor;
    }

    public void setAssignToRequestor(boolean assignToRequestor) {
        this.assignToRequestor = assignToRequestor;
    }

    public String getWorkItemType() {
        return workItemType;
    }

    public void setWorkItemType(String workItemType) {
        this.workItemType = workItemType;
    }

    public String getMultipliers() {
        return multipliers;
    }

    public void setMultipliers(String multipliers) {
        this.multipliers = multipliers;
    }

    public boolean isParallel() {
        return parallel;
    }

    public void setParallel(boolean parallel) {
        this.parallel = parallel;
    }

    public boolean isContinueOnError() {
        return continueOnError;
    }

    public void setContinueOnError(boolean continueOnError) {
        this.continueOnError = continueOnError;
    }

    public String getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(String additionalFields) {
        this.additionalFields = additionalFields;
    }
}
