package io.quind.repository;

import io.quind.model.quind.applications.Application;
import io.quind.model.quind.applications.ApplicationConfig;
import io.quind.repository.QuindRepository;
import io.quind.rest.RestClient;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

@SuppressWarnings("unchecked")
public class QuindRepositoryTest {

    @Mock
    RestClient restClient;

    QuindRepository quindRepository;

    public QuindRepositoryTest() {
        MockitoAnnotations.initMocks(this);
        quindRepository = new QuindRepository(restClient, "http://cilogstaging.quind.io");
    }

    @Test
    public void shouldGetApiClients() {
        List<Integer> expectedApiClients = new ArrayList<Integer>(Arrays.asList(3));
        final Response response = Mockito.mock(Response.class);

        Mockito.when(response.readEntity(Mockito.any(Class.class))).thenReturn(expectedApiClients);
        doReturn(response).when(restClient).doGet("http://cilogstaging.quind.io/api/v1/api-clients/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9/roots");

        List<Integer> apiClients = quindRepository.getApiClients("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9");
        assertEquals(expectedApiClients.toString(), apiClients.toString());
    }

    @Test
    public void shouldGetProviderInformation() {
        List<ApplicationConfig> expectedConfiguracionesAplicaciones = Arrays.asList(new ApplicationConfig("5", "DefaultCollection/Pruebas", ".*Pruebas.*", "https://satrackvsts.visualstudio.com", "https://satrackvsts.vsrm.visualstudio.com", 1, "daniel.betancur@satrack.com", "qz73dfdiqylufrkjou5a7mbueeh255vsxmnflde6yr6opngphdsa", "daniel.betancur@satrack.com", "qz73dfdiqylufrkjou5a7mbueeh255vsxmnflde6yr6opngphdsa", null));
        final Response response = Mockito.mock(Response.class);

        Mockito.when(response.readEntity(Mockito.any(GenericType.class))).thenReturn(expectedConfiguracionesAplicaciones);
        doReturn(response).when(restClient).doGet("http://cilogstaging.quind.io/api/v1/components/3/leaves");

        List<ApplicationConfig> configuracionesAplicaciones = quindRepository.getProviderConfiguration(3);
        assertEquals(expectedConfiguracionesAplicaciones, configuracionesAplicaciones);
    }

    @Test
    public void shouldSendJobDefinitions() {
        List<Application> applications = Arrays.asList(new Application(), new Application());
        final Response response = Mockito.mock(Response.class);

        Mockito.when(response.getStatus()).thenReturn(201);
        doReturn(response).when(restClient).doPost(Mockito.anyString(), Mockito.anyObject());

        boolean result = quindRepository.enviarJobDefinitions(applications);
        assertTrue(result);
    }

    @Test
    public void shouldFinishProcess() {
        final Response response = Mockito.mock(Response.class);

        Mockito.when(response.getStatus()).thenReturn(200);
        doReturn(response).when(restClient).doPost(Mockito.anyString(), Mockito.anyObject());

        boolean result = quindRepository.finishProcess(3);
        assertTrue(result);
    }
}
