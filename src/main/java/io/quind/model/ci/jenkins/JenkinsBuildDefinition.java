package io.quind.model.ci.jenkins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JenkinsBuildDefinition {

    private String _class;
    private String fullDisplayName;
    private List<Action> actions;

    public JenkinsBuildDefinition() {
    }

    public JenkinsBuildDefinition(String _class, String fullDisplayName, List<Action> actions) {
        this._class = _class;
        this.fullDisplayName = fullDisplayName;
        this.actions = actions;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getFullDisplayName() {
        return fullDisplayName;
    }

    public void setFullDisplayName(String fullDisplayName) {
        this.fullDisplayName = fullDisplayName;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
