package io.quind.model.quind.applications;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quind.model.quind.jobs.JobDefinition;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Application {

    private String id;
    private List<JobDefinition> jobDefinitions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<JobDefinition> getJobDefinitions() {
        return jobDefinitions;
    }

    public void setJobDefinitions(List<JobDefinition> jobDefinitions) {
        this.jobDefinitions = jobDefinitions;
    }


}
